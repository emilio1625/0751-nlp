---
author: Emilio Cabrera
jupyter:
    nbformat: 4
    nbformat_minor: 5
    kernelspec:
        display_name: Julia 1.2.0
        language: julia
        name: julia-1.2
    language_info:
        file_extension: .jl
        mimetype: application/julia
        name: julia
        version: 1.2.0
---
<div class="cell code" data-execution_count="1">

``` julia
# Instala lo necesario, descomente la siguiente linea (solo el #)
#] add CSV
```

</div>

<div class="cell code" data-execution_count="2">

``` julia
##############################################################
# En julia los indices de arreglos y matrices comienzan en 1 #
##############################################################

using CSV, Random
O_sent = Array{AbstractString, 1}() # cadenas de observaciones
S_sent = Array{AbstractString, 1}() # cadenas de emisiones

# Cargamos el corpus

# obs = ["el salto de altura", "yo salto la cuerda",
#         "ellos tomaban vino", "ellos saltaban la cuerda"]
# ems = ["DA NC PP NC", "DP V DA NC", "DP V NC", "DP V DA NC"]
#obs = ["el gato come", "el gato duerme", "el perro come"]
#ems = ["DA N V", "DA N V", "DA N V"]
rows = shuffle(collect(CSV.Rows("corpus.csv", header=0, delim='\t')))
#rows = collect(CSV.Rows("corpus.csv", header=0, delim='\t'))
for row in rows
#for row in zip(obs, ems)
    o = lowercase(row[1])
    push!(O_sent, o)
    s = "<BOS> " * row[2] * " <EOS>"
    push!(S_sent, s)
end
```

</div>

<div class="cell code" data-execution_count="3">

``` julia
# dividimos los datos

part = Int(round(length(O_sent)*0.7))
O_sent, O_sent_test = O_sent[1:part], O_sent[part+1:end]
S_sent, S_sent_test = S_sent[1:part], S_sent[part+1:end];
```

</div>

<div class="cell code" data-execution_count="4">

``` julia
# Generamos los pares de obs-em
O_S = Array{Tuple{String, String}, 1}() # pares obs-em como cadenas
for (o, s) in zip(O_sent, S_sent)
    push!(O_S, collect(zip(split(o), split(s)[2:end-1]))...)
end
```

</div>

<div class="cell code" data-execution_count="5">

``` julia
# genera los indices para el corpus
function genidx(corpus)
    vocab = Dict{AbstractString, Int64}()
    words = Iterators.flatten(split(sent) for sent in corpus)
    list_idx = AbstractString[]
    # <BOS> y <EOS> se agregan despues
    for word in setdiff(Set(words), ["<BOS>", "<EOS>"])
        vocab[word] = length(vocab) + 1
        push!(list_idx, word)
    end
    return (vocab, list_idx)
end

# Dada una cadena devuelve el los indices, o "<UNK>" si no existe en el vocabulario
function txt2idx(corpus, vocab)
    idxs = Array{Array{Int64, 1}, 1}()
    for doc in corpus
        push!(idxs, [get(vocab, w, get(vocab, "<UNK>", 0)) for w in split(doc)])
    end
    return idxs
end
```

<div class="output execute_result" data-execution_count="5">

    txt2idx (generic function with 1 method)

</div>

</div>

<div class="cell code" data-execution_count="6" data-scrolled="false">

``` julia
# Generamos indices para los vocabularios
O_dic, O_inv = genidx(O_sent)
O_dic["<UNK>"] = length(O_dic) + 1

S_dic, S_inv = genidx(S_sent)
S_dic["<EOS>"] = length(S_dic) + 1
S_dic["<BOS>"] = length(S_dic) + 1;
#display(O_dic)
#display(S_dic)
```

</div>

<div class="cell code" data-execution_count="7">

``` julia
# transformamos las sentencias en arreglos de sus indices
O_idx = txt2idx(O_sent, O_dic)
# display(O_idx)
# lo mismo para las producciones
S_idx = txt2idx(S_sent, S_dic)
# display(S_idx)
# formamos pares de observaciones y emisiones a pares de indices
OS_idx = collect((O_dic[w[1]], S_dic[w[2]]) for w in O_S);
```

</div>

<div class="cell code" data-execution_count="8">

``` julia
# Obtenemos los bigramas
S_bi = vcat((collect(zip(cad[1:end-1], cad[2:end])) for cad in S_idx)...);
```

</div>

<div class="cell code" data-execution_count="9">

``` julia
# Cuando cuentes cuentos
# cuenta cuántos cuentos cuentas,
# porque si no cuentas cuántos cuentos cuentas,
# nunca sabrás cuántos cuentos has contado tú.
function counter(items)
    counts = Dict{typeof(items[1]), Int64}()
    for item in items
        counts[item]= Int64(get(counts, item, 0) + 1)
    end
    return counts
end
```

<div class="output execute_result" data-execution_count="9">

    counter (generic function with 1 method)

</div>

</div>

<div class="cell code" data-execution_count="10">

``` julia
# Matrices del HMM
N = length(S_dic)
M = length(O_dic)

Π = zeros(N-2)
A = zeros(N-2, N-1)
B = zeros(M, N-2)

#Π = zeros(Rational{Int64}, N-2)
#A = zeros(Rational{Int64}, N-2, N-1)
#B = zeros(Rational{Int64}, M, N-2);
```

</div>

<div class="cell code" data-execution_count="11">

``` julia
# Frecuencias en las producciones
freqs = counter(S_bi)
#display(freqs)
#display(S_dic)
for (bi, freq) in freqs
    if bi[1] == S_dic["<BOS>"]
        Π[bi[2]] = freq
    else
        A[bi...] = freq
    end
end
```

</div>

<div class="cell code" data-execution_count="12">

``` julia
#display(Π)
#display(A)
```

</div>

<div class="cell code" data-execution_count="13">

``` julia
Σ=sum
# Smoothing Laplaciano
A = (A .+ 1) ./ Σ(A .+ 1, dims=2)
Π = (Π .+ 1) ./ Σ(Π .+ 1, dims=1);
```

</div>

<div class="cell code" data-execution_count="14">

``` julia
#display(Π)
#display(A)
```

</div>

<div class="cell code" data-execution_count="15">

``` julia
# Comprobamos que sumen 1
#print(Σ(A, dims=2), Σ(Π, dims=1))
```

</div>

<div class="cell code" data-execution_count="16">

``` julia
# Frecuencias en los pares
freqs = counter(OS_idx)

for (pair, freq) in freqs
    B[pair...] = freq
end
# Smothing
B = (B .+ 1) ./ Σ(B .+ 1, dims=1);
```

</div>

<div class="cell code" data-execution_count="17">

``` julia
# Comprobamos que sumen 1
# print(Σ(B, dims=1))
```

</div>

<div class="cell code" data-execution_count="18">

``` julia
# dada una cadena (de emisiones) devuelve su probabilidad
function probcad(str)
    seq = txt2idx([str], S_dic)[1]
    p = Π[seq[1]]
    for i in 1:length(seq)-1
        p *= A[seq[i], seq[i+1]]
    end
    return p
end
```

<div class="output execute_result" data-execution_count="18">

    probcad (generic function with 1 method)

</div>

</div>

<div class="cell code" data-execution_count="19">

``` julia
# Ejemplo Notebook Modelos de Markov
# probcad("DA NC V")
```

</div>

<div class="cell code" data-execution_count="20">

``` julia
# :D
# devuelve las emisiones mas probables para una cadena de
# observaciones como indices y como arreglo de cadenas
function viterbi(obs::AbstractString)
    o = txt2idx([obs], O_dic)[1]

    # Avance
    ## Inicializacion
    ϕ = []
    δ = []
    #   p(o(1) | sj) ⊙ Π
    probs = B[o[1], :] .* Π
    δj = maximum(probs, dims=2)
    push!(δ, δj)
    push!(ϕ, 0) # solo para rellenar

    ## Induccion
    for t in 1:length(o)-1
        #       p(o(t) | sj)    p(sj | si)       δi(t)
        probs = B[o[t+1], :] .* A[:, 1:end-1]' .* δ[t]'
        #                            ^~~~~~^ elimina columna <EOS>
        push!(δ, maximum(probs, dims=2))
        push!(ϕ, argmax(probs, dims=2))
    end

    # Retroceso

    ## Inicializacion
    s = zeros(Int64, length(o))
    T = length(o)
    δj = argmax(δ[T])[1]
    #                 ^~~ argmax regresa renglon y columna, solo nos interesa el reglon
    s[T] = δj

    ## Induccion
    for t = length(o):-1:2
        s[t-1] = ϕ[t][s[t]][2]
        #                   ^~~ solo nos interesa la columna
    end

    # Finalizacion
    return s, map((x) -> S_inv[x], s)

end
```

<div class="output execute_result" data-execution_count="20">

    viterbi (generic function with 1 method)

</div>

</div>

<div class="cell code" data-execution_count="21">

``` julia
# Prueba ejemplo de Notebook Modelos Ocultos de Markov
# @time viterbi("yo salto el salto")
```

</div>

<div class="cell code" data-execution_count="22">

``` julia
# Ejemplo de clase
# @time viterbi("el perro duerme")
```

</div>

<div class="cell code" data-execution_count="23">

``` julia
# Ejemplos
# la primer ejecucion de una funcion es lenta en Julia
# corridas posteriores son mucho más rapidas
@time viterbi("Jámas hubiera sido capa de coronarse solo")[2]
```

<div class="output stream stdout">

```
  2.425694 seconds (4.46 M allocations: 223.326 MiB, 3.85% gc time)
```

</div>

<div class="output execute_result" data-execution_count="23">

    7-element Array{SubString{String},1}:
     "DA0MS0"
     "NCMS000"
     "SPCMS"
     "NCMS000"
     "SPS00"
     "DA0FS0"
     "NCFS000"

</div>

</div>

<div class="cell code" data-execution_count="24">

``` julia
@time viterbi("Los árboles de otoño susurran suavemente")[2]
```

<div class="output stream stdout">

```
  0.003154 seconds (313 allocations: 1.646 MiB)
```

</div>

<div class="output execute_result" data-execution_count="24">

```
6-element Array{SubString{String},1}:
 "DA0MS0"
 "NCMS000"
 "SPS00"
 "DA0FS0"
 "NCFS000"
 "SPS00"
```

</div>

</div>

<div class="cell code" data-execution_count="25">

``` julia
@time viterbi("En algun lugar de la mancha de cuyo nombre no quiero acordarme")[2]
```

<div class="output stream stdout">

```
  0.017358 seconds (646 allocations: 3.616 MiB, 48.77% gc time)
```

</div>

<div class="output execute_result" data-execution_count="25">

    12-element Array{SubString{String},1}:
     "SPS00"
     "DA0MS0"
     "NCMS000"
     "SPS00"
     "DA0FS0"
     "NCFS000"
     "SPS00"
     "DA0FS0"
     "NCFS000"
     "SPS00"
     "DA0FS0"
     "NCFS000"

</div>

</div>

<div class="cell code" data-execution_count="26">

``` julia
@time viterbi("Cuando cuentes cuentos cuenta cuántos cuentos cuentas, porque si no cuentas cuántos cuentos cuentas, nunca sabrás cuántos cuentos has contado tú.")[2]
```

<div class="output stream stdout">

```
  0.015020 seconds (1.14 k allocations: 6.571 MiB)
```

</div>

<div class="output execute_result" data-execution_count="26">

```
21-element Array{SubString{String},1}:
 "DA0MS0"
 "NCMS000"
 "SPS00"
 "DA0FS0"
 "NCFS000"
 "SPS00"
 "DA0FS0"
 "NCFS000"
 "SPS00"
 "DA0FS0"
 "NCFS000"
 "SPS00"
 "DA0FS0"
 "NCFS000"
 "SPS00"
 "DA0FS0"
 "NCFS000"
 "SPS00"
 "DA0FS0"
 "NCFS000"
 "SPS00"
```

</div>

</div>

<div class="cell code" data-execution_count="27" data-scrolled="false">

``` julia
acc = 0.0

@time for (o, s) in zip(O_sent_test, S_sent_test)
    pred = viterbi(o)[2]
    act  = split(s)[2:end-1]
    if length(pred) != length(act)
        error("Algo anda mal D;")
    end
    global acc += sum(pred .== act) / length(pred)
end
acc = acc / length(O_sent_test)
print("Accuracy: ", acc * 100, "%")
```

<div class="output stream stdout">

```
  1.137999 seconds (516.49 k allocations: 867.455 MiB, 5.07% gc time)
Accuracy: 57.356837692352734%
```

</div>

</div>
